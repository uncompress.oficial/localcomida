﻿Imports System.Data.SqlClient

Public Class addProducts

    Dim con As New SqlConnection
    Dim cmd As New SqlCommand
    Dim i As Integer


    Private Sub addProducts_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\colacionesAmuyen.mdf;Integrated Security=True;Connect Timeout=30"
        If con.State = ConnectionState.Open Then
            con.Close()
        End If
        con.Open()
        mostrar_productos()

    End Sub


    Private Sub btnAgregarProducto_Click(sender As Object, e As EventArgs) Handles cboProducto.Click
        Dim cont As Integer

        If txtNombre.Text.Trim.Equals("") Then
            cont += 1
        End If
        If txtValor.Text.Trim.Equals("") Then
            cont += 1
        End If


        If cont = 0 Then
            cmd = con.CreateCommand()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "insert into Product values('" + txtNombre.Text + "','" + txtValor.Text + "' , '" + cboProductos.SelectedItem + "', '1')"
            cmd.ExecuteNonQuery()

            MessageBox.Show("PRODUCTO AGREGADO")
            mostrar_productos()
            txtNombre.Text = ""
            txtValor.Text = ""

        Else
            MessageBox.Show("COMPLETAR TODOS LOS CAMPOS")
        End If

    End Sub


    Public Sub mostrar_productos()

        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "select id as ID, name as 'NOMBRE DEL PRODUCTO', price as PRECIO, type as 'TIPO DE PRODUCTO' from Product where status = 1 And type != 'oferta'"
        cmd.ExecuteNonQuery()
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt)
        dgProducto.DataSource = dt

    End Sub

End Class