﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMenu))
        Me.PanelSlideMenu = New System.Windows.Forms.Panel()
        Me.btnStatistics = New System.Windows.Forms.Button()
        Me.PanelModifySubmenu = New System.Windows.Forms.Panel()
        Me.btnModifyOffers = New System.Windows.Forms.Button()
        Me.btnModifyProducts = New System.Windows.Forms.Button()
        Me.btnModify = New System.Windows.Forms.Button()
        Me.PanelAddSubmenu = New System.Windows.Forms.Panel()
        Me.btnAddOffer = New System.Windows.Forms.Button()
        Me.btnAddProduct = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.PanelSalesSubmenu = New System.Windows.Forms.Panel()
        Me.btnModifySales = New System.Windows.Forms.Button()
        Me.btnMakeSales = New System.Windows.Forms.Button()
        Me.btnSales = New System.Windows.Forms.Button()
        Me.PanelLogo = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblhora = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.horaFecha = New System.Windows.Forms.Timer(Me.components)
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PanelSlideMenu.SuspendLayout()
        Me.PanelModifySubmenu.SuspendLayout()
        Me.PanelAddSubmenu.SuspendLayout()
        Me.PanelSalesSubmenu.SuspendLayout()
        Me.PanelLogo.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelSlideMenu
        '
        Me.PanelSlideMenu.AutoScroll = True
        Me.PanelSlideMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(11, Byte), Integer), CType(CType(7, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.PanelSlideMenu.Controls.Add(Me.btnStatistics)
        Me.PanelSlideMenu.Controls.Add(Me.PanelModifySubmenu)
        Me.PanelSlideMenu.Controls.Add(Me.btnModify)
        Me.PanelSlideMenu.Controls.Add(Me.PanelAddSubmenu)
        Me.PanelSlideMenu.Controls.Add(Me.btnAdd)
        Me.PanelSlideMenu.Controls.Add(Me.PanelSalesSubmenu)
        Me.PanelSlideMenu.Controls.Add(Me.btnSales)
        Me.PanelSlideMenu.Controls.Add(Me.PanelLogo)
        Me.PanelSlideMenu.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelSlideMenu.Location = New System.Drawing.Point(0, 0)
        Me.PanelSlideMenu.Name = "PanelSlideMenu"
        Me.PanelSlideMenu.Size = New System.Drawing.Size(200, 761)
        Me.PanelSlideMenu.TabIndex = 0
        '
        'btnStatistics
        '
        Me.btnStatistics.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnStatistics.FlatAppearance.BorderSize = 0
        Me.btnStatistics.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.btnStatistics.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStatistics.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnStatistics.Location = New System.Drawing.Point(0, 487)
        Me.btnStatistics.Name = "btnStatistics"
        Me.btnStatistics.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.btnStatistics.Size = New System.Drawing.Size(200, 45)
        Me.btnStatistics.TabIndex = 7
        Me.btnStatistics.Text = "Estadisticas"
        Me.btnStatistics.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStatistics.UseVisualStyleBackColor = True
        '
        'PanelModifySubmenu
        '
        Me.PanelModifySubmenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.PanelModifySubmenu.Controls.Add(Me.btnModifyOffers)
        Me.PanelModifySubmenu.Controls.Add(Me.btnModifyProducts)
        Me.PanelModifySubmenu.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelModifySubmenu.Location = New System.Drawing.Point(0, 403)
        Me.PanelModifySubmenu.Name = "PanelModifySubmenu"
        Me.PanelModifySubmenu.Size = New System.Drawing.Size(200, 84)
        Me.PanelModifySubmenu.TabIndex = 6
        '
        'btnModifyOffers
        '
        Me.btnModifyOffers.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnModifyOffers.FlatAppearance.BorderSize = 0
        Me.btnModifyOffers.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnModifyOffers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnModifyOffers.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModifyOffers.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnModifyOffers.Location = New System.Drawing.Point(0, 40)
        Me.btnModifyOffers.Name = "btnModifyOffers"
        Me.btnModifyOffers.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.btnModifyOffers.Size = New System.Drawing.Size(200, 40)
        Me.btnModifyOffers.TabIndex = 1
        Me.btnModifyOffers.Text = "Modificar Ofertas"
        Me.btnModifyOffers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModifyOffers.UseVisualStyleBackColor = True
        '
        'btnModifyProducts
        '
        Me.btnModifyProducts.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnModifyProducts.FlatAppearance.BorderSize = 0
        Me.btnModifyProducts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnModifyProducts.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnModifyProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModifyProducts.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnModifyProducts.Location = New System.Drawing.Point(0, 0)
        Me.btnModifyProducts.Name = "btnModifyProducts"
        Me.btnModifyProducts.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.btnModifyProducts.Size = New System.Drawing.Size(200, 40)
        Me.btnModifyProducts.TabIndex = 0
        Me.btnModifyProducts.Text = "Modificar Productos"
        Me.btnModifyProducts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModifyProducts.UseVisualStyleBackColor = True
        '
        'btnModify
        '
        Me.btnModify.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnModify.FlatAppearance.BorderSize = 0
        Me.btnModify.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.btnModify.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.btnModify.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModify.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnModify.Location = New System.Drawing.Point(0, 358)
        Me.btnModify.Name = "btnModify"
        Me.btnModify.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.btnModify.Size = New System.Drawing.Size(200, 45)
        Me.btnModify.TabIndex = 5
        Me.btnModify.Text = "Modificar"
        Me.btnModify.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModify.UseVisualStyleBackColor = True
        '
        'PanelAddSubmenu
        '
        Me.PanelAddSubmenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.PanelAddSubmenu.Controls.Add(Me.btnAddOffer)
        Me.PanelAddSubmenu.Controls.Add(Me.btnAddProduct)
        Me.PanelAddSubmenu.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelAddSubmenu.Location = New System.Drawing.Point(0, 274)
        Me.PanelAddSubmenu.Name = "PanelAddSubmenu"
        Me.PanelAddSubmenu.Size = New System.Drawing.Size(200, 84)
        Me.PanelAddSubmenu.TabIndex = 4
        '
        'btnAddOffer
        '
        Me.btnAddOffer.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnAddOffer.FlatAppearance.BorderSize = 0
        Me.btnAddOffer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnAddOffer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnAddOffer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddOffer.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnAddOffer.Location = New System.Drawing.Point(0, 40)
        Me.btnAddOffer.Name = "btnAddOffer"
        Me.btnAddOffer.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.btnAddOffer.Size = New System.Drawing.Size(200, 40)
        Me.btnAddOffer.TabIndex = 1
        Me.btnAddOffer.Text = "Agregar Oferta"
        Me.btnAddOffer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAddOffer.UseVisualStyleBackColor = True
        '
        'btnAddProduct
        '
        Me.btnAddProduct.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnAddProduct.FlatAppearance.BorderSize = 0
        Me.btnAddProduct.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnAddProduct.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnAddProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddProduct.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnAddProduct.Location = New System.Drawing.Point(0, 0)
        Me.btnAddProduct.Name = "btnAddProduct"
        Me.btnAddProduct.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.btnAddProduct.Size = New System.Drawing.Size(200, 40)
        Me.btnAddProduct.TabIndex = 0
        Me.btnAddProduct.Text = "Agregar Producto"
        Me.btnAddProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAddProduct.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnAdd.Location = New System.Drawing.Point(0, 229)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.btnAdd.Size = New System.Drawing.Size(200, 45)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.Text = "Agregar"
        Me.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'PanelSalesSubmenu
        '
        Me.PanelSalesSubmenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.PanelSalesSubmenu.Controls.Add(Me.btnModifySales)
        Me.PanelSalesSubmenu.Controls.Add(Me.btnMakeSales)
        Me.PanelSalesSubmenu.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelSalesSubmenu.Location = New System.Drawing.Point(0, 145)
        Me.PanelSalesSubmenu.Name = "PanelSalesSubmenu"
        Me.PanelSalesSubmenu.Size = New System.Drawing.Size(200, 84)
        Me.PanelSalesSubmenu.TabIndex = 2
        '
        'btnModifySales
        '
        Me.btnModifySales.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnModifySales.FlatAppearance.BorderSize = 0
        Me.btnModifySales.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnModifySales.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnModifySales.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModifySales.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnModifySales.Location = New System.Drawing.Point(0, 40)
        Me.btnModifySales.Name = "btnModifySales"
        Me.btnModifySales.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.btnModifySales.Size = New System.Drawing.Size(200, 40)
        Me.btnModifySales.TabIndex = 1
        Me.btnModifySales.Text = "Modificar Venta"
        Me.btnModifySales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModifySales.UseVisualStyleBackColor = True
        '
        'btnMakeSales
        '
        Me.btnMakeSales.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnMakeSales.FlatAppearance.BorderSize = 0
        Me.btnMakeSales.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnMakeSales.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnMakeSales.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMakeSales.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnMakeSales.Location = New System.Drawing.Point(0, 0)
        Me.btnMakeSales.Name = "btnMakeSales"
        Me.btnMakeSales.Padding = New System.Windows.Forms.Padding(35, 0, 0, 0)
        Me.btnMakeSales.Size = New System.Drawing.Size(200, 40)
        Me.btnMakeSales.TabIndex = 0
        Me.btnMakeSales.Text = "Realizar Venta"
        Me.btnMakeSales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnMakeSales.UseVisualStyleBackColor = True
        '
        'btnSales
        '
        Me.btnSales.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSales.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.btnSales.FlatAppearance.BorderSize = 0
        Me.btnSales.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(21, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.btnSales.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(24, Byte), Integer), CType(CType(22, Byte), Integer), CType(CType(34, Byte), Integer))
        Me.btnSales.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSales.ForeColor = System.Drawing.Color.Silver
        Me.btnSales.Location = New System.Drawing.Point(0, 100)
        Me.btnSales.Name = "btnSales"
        Me.btnSales.Padding = New System.Windows.Forms.Padding(10, 0, 0, 0)
        Me.btnSales.Size = New System.Drawing.Size(200, 45)
        Me.btnSales.TabIndex = 1
        Me.btnSales.Text = "Ventas"
        Me.btnSales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSales.UseVisualStyleBackColor = True
        '
        'PanelLogo
        '
        Me.PanelLogo.Controls.Add(Me.PictureBox1)
        Me.PanelLogo.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelLogo.Location = New System.Drawing.Point(0, 0)
        Me.PanelLogo.Name = "PanelLogo"
        Me.PanelLogo.Size = New System.Drawing.Size(200, 100)
        Me.PanelLogo.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(34, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(129, 69)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'lblhora
        '
        Me.lblhora.AutoSize = True
        Me.lblhora.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblhora.ForeColor = System.Drawing.Color.Silver
        Me.lblhora.Location = New System.Drawing.Point(672, 326)
        Me.lblhora.Name = "lblhora"
        Me.lblhora.Size = New System.Drawing.Size(101, 31)
        Me.lblhora.TabIndex = 1
        Me.lblhora.Text = "Label1"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.ForeColor = System.Drawing.Color.Silver
        Me.lblFecha.Location = New System.Drawing.Point(576, 382)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(101, 31)
        Me.lblFecha.TabIndex = 2
        Me.lblFecha.Text = "Label2"
        '
        'horaFecha
        '
        Me.horaFecha.Enabled = True
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(554, 100)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(346, 155)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 3
        Me.PictureBox3.TabStop = False
        '
        'FormMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(45, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1284, 761)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblhora)
        Me.Controls.Add(Me.PanelSlideMenu)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MinimumSize = New System.Drawing.Size(1300, 800)
        Me.Name = "FormMenu"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu"
        Me.TransparencyKey = System.Drawing.Color.Transparent
        Me.PanelSlideMenu.ResumeLayout(False)
        Me.PanelModifySubmenu.ResumeLayout(False)
        Me.PanelAddSubmenu.ResumeLayout(False)
        Me.PanelSalesSubmenu.ResumeLayout(False)
        Me.PanelLogo.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PanelSlideMenu As Panel
    Friend WithEvents btnSales As Button
    Friend WithEvents PanelLogo As Panel
    Friend WithEvents btnStatistics As Button
    Friend WithEvents PanelModifySubmenu As Panel
    Friend WithEvents btnModifyOffers As Button
    Friend WithEvents btnModifyProducts As Button
    Friend WithEvents btnModify As Button
    Friend WithEvents PanelAddSubmenu As Panel
    Friend WithEvents btnAddOffer As Button
    Friend WithEvents btnAddProduct As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents PanelSalesSubmenu As Panel
    Friend WithEvents btnModifySales As Button
    Friend WithEvents btnMakeSales As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents lblhora As Label
    Friend WithEvents lblFecha As Label
    Friend WithEvents horaFecha As Timer
    Friend WithEvents PictureBox3 As PictureBox
End Class
