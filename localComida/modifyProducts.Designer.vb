﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class modifyProducts
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnEliminarProducto = New System.Windows.Forms.Button()
        Me.btnModifiarProdcuto = New System.Windows.Forms.Button()
        Me.dgModificarProducto = New System.Windows.Forms.DataGridView()
        CType(Me.dgModificarProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnEliminarProducto
        '
        Me.btnEliminarProducto.Location = New System.Drawing.Point(1090, 367)
        Me.btnEliminarProducto.Name = "btnEliminarProducto"
        Me.btnEliminarProducto.Size = New System.Drawing.Size(116, 62)
        Me.btnEliminarProducto.TabIndex = 7
        Me.btnEliminarProducto.Text = "ELIMINAR"
        Me.btnEliminarProducto.UseVisualStyleBackColor = True
        '
        'btnModifiarProdcuto
        '
        Me.btnModifiarProdcuto.Location = New System.Drawing.Point(90, 367)
        Me.btnModifiarProdcuto.Name = "btnModifiarProdcuto"
        Me.btnModifiarProdcuto.Size = New System.Drawing.Size(116, 62)
        Me.btnModifiarProdcuto.TabIndex = 5
        Me.btnModifiarProdcuto.Text = "MODIFICAR"
        Me.btnModifiarProdcuto.UseVisualStyleBackColor = True
        '
        'dgModificarProducto
        '
        Me.dgModificarProducto.AllowUserToAddRows = False
        Me.dgModificarProducto.AllowUserToDeleteRows = False
        Me.dgModificarProducto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgModificarProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgModificarProducto.Location = New System.Drawing.Point(90, 48)
        Me.dgModificarProducto.Name = "dgModificarProducto"
        Me.dgModificarProducto.Size = New System.Drawing.Size(1116, 206)
        Me.dgModificarProducto.TabIndex = 4
        '
        'modifyProducts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(45, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1284, 761)
        Me.Controls.Add(Me.btnEliminarProducto)
        Me.Controls.Add(Me.btnModifiarProdcuto)
        Me.Controls.Add(Me.dgModificarProducto)
        Me.MinimumSize = New System.Drawing.Size(1300, 800)
        Me.Name = "modifyProducts"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MODIFICAR PRODUCTO"
        CType(Me.dgModificarProducto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnEliminarProducto As Button
    Friend WithEvents btnModifiarProdcuto As Button
    Friend WithEvents dgModificarProducto As DataGridView
End Class
