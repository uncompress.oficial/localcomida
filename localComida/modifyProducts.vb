﻿Imports System.Data.SqlClient

Public Class modifyProducts

    Dim con As New SqlConnection
    Dim cmd As New SqlCommand
    Dim builder As SqlCommandBuilder
    Dim adap As SqlDataAdapter
    Dim dt As DataTable
    Dim i As Integer


    Private Sub modifyProducts_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\colacionesAmuyen.mdf;Integrated Security=True;Connect Timeout=30"
        If con.State = ConnectionState.Open Then
            con.Close()
        End If
        con.Open()
        mostrar_productos()
    End Sub



    Private Sub btnModifiarProdcuto_Click(sender As Object, e As EventArgs) Handles btnModifiarProdcuto.Click
        'Dim Sql As String = "UPDATE Product SET name = '" & dgModificarProducto.CurrentRow.Cells(1).Value & "' , price = '" & dgModificarProducto.CurrentRow.Cells(1).Value & "'  WHERE id = '" & dgModificarProducto.CurrentRow.Cells(0).Value & "'"

        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "UPDATE Product SET name = '" & dgModificarProducto.CurrentRow.Cells(1).Value.ToString.ToUpper & "' , price = '" & dgModificarProducto.CurrentRow.Cells(2).Value & "'  WHERE id = '" & dgModificarProducto.CurrentRow.Cells(0).Value & "'"
        cmd.ExecuteNonQuery()

        MessageBox.Show("PRODUCTO MODIFICADO")
        mostrar_productos()

    End Sub

    Public Sub mostrar_productos()

        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "select id as ID, name as 'NOMBRE DEL PRODUCTO', price as PRECIO, type as 'TIPO DE PRODUCTO' from Product where status = 1 And type != 'oferta'"
        cmd.ExecuteNonQuery()
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt)
        dgModificarProducto.DataSource = dt

    End Sub

    Private Sub btnEliminarProducto_Click(sender As Object, e As EventArgs) Handles btnEliminarProducto.Click
        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        If MessageBox.Show("SEGURO QUE DESEA ELIMINAR?", "ELIMINAR PRODUCTO", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            cmd.CommandText = "UPDATE Product SET status = 0  WHERE id = '" & dgModificarProducto.CurrentRow.Cells(0).Value & "'"
            cmd.ExecuteNonQuery()
            MessageBox.Show("PRDUCTO ELIMINADO")
            mostrar_productos()
        End If

    End Sub


End Class
