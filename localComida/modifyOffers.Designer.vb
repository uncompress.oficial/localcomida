﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class modifyOffers
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgEliminarOfertas = New System.Windows.Forms.DataGridView()
        Me.btnModificarOferta = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        CType(Me.dgEliminarOfertas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgEliminarOfertas
        '
        Me.dgEliminarOfertas.AllowUserToAddRows = False
        Me.dgEliminarOfertas.AllowUserToDeleteRows = False
        Me.dgEliminarOfertas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgEliminarOfertas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEliminarOfertas.Location = New System.Drawing.Point(74, 50)
        Me.dgEliminarOfertas.Name = "dgEliminarOfertas"
        Me.dgEliminarOfertas.Size = New System.Drawing.Size(1116, 206)
        Me.dgEliminarOfertas.TabIndex = 0
        '
        'btnModificarOferta
        '
        Me.btnModificarOferta.Location = New System.Drawing.Point(74, 369)
        Me.btnModificarOferta.Name = "btnModificarOferta"
        Me.btnModificarOferta.Size = New System.Drawing.Size(116, 62)
        Me.btnModificarOferta.TabIndex = 1
        Me.btnModificarOferta.Text = "MODIFICAR"
        Me.btnModificarOferta.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(1074, 369)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(116, 62)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Text = "ELIMINAR"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'modifyOffers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(45, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1284, 761)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnModificarOferta)
        Me.Controls.Add(Me.dgEliminarOfertas)
        Me.MinimumSize = New System.Drawing.Size(1300, 800)
        Me.Name = "modifyOffers"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MODIFICAR OFERTA"
        CType(Me.dgEliminarOfertas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgEliminarOfertas As DataGridView
    Friend WithEvents btnModificarOferta As Button
    Friend WithEvents btnEliminar As Button
End Class
