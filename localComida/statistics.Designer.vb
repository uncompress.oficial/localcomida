﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class statistics
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnLimpiar = New System.Windows.Forms.Button()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgEstadisticas = New System.Windows.Forms.DataGridView()
        Me.cboPaymentType = New System.Windows.Forms.ComboBox()
        Me.cboTipoEntrega = New System.Windows.Forms.ComboBox()
        Me.txtFechaFin = New System.Windows.Forms.MaskedTextBox()
        Me.txtFechaIni = New System.Windows.Forms.MaskedTextBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        CType(Me.dgEstadisticas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Location = New System.Drawing.Point(72, 579)
        Me.btnLimpiar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(155, 52)
        Me.btnLimpiar.TabIndex = 27
        Me.btnLimpiar.Text = "LIMPIAR FILTROS"
        Me.btnLimpiar.UseVisualStyleBackColor = True
        '
        'txtTotal
        '
        Me.txtTotal.Enabled = False
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(1036, 493)
        Me.txtTotal.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(155, 26)
        Me.txtTotal.TabIndex = 26
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.Silver
        Me.Label5.Location = New System.Drawing.Point(951, 501)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "TOTAL"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Silver
        Me.Label4.Location = New System.Drawing.Point(779, 53)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "FORMA DE PAGO"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Silver
        Me.Label3.Location = New System.Drawing.Point(469, 51)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 13)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "TIPO DE ENTREGA"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Silver
        Me.Label2.Location = New System.Drawing.Point(269, 51)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 13)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "FECHA FINAL"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Silver
        Me.Label1.Location = New System.Drawing.Point(69, 51)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "FECHA INICIAL"
        '
        'dgEstadisticas
        '
        Me.dgEstadisticas.AllowUserToAddRows = False
        Me.dgEstadisticas.AllowUserToDeleteRows = False
        Me.dgEstadisticas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgEstadisticas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgEstadisticas.Location = New System.Drawing.Point(71, 139)
        Me.dgEstadisticas.Margin = New System.Windows.Forms.Padding(2)
        Me.dgEstadisticas.Name = "dgEstadisticas"
        Me.dgEstadisticas.ReadOnly = True
        Me.dgEstadisticas.RowHeadersWidth = 51
        Me.dgEstadisticas.RowTemplate.Height = 24
        Me.dgEstadisticas.Size = New System.Drawing.Size(1120, 306)
        Me.dgEstadisticas.TabIndex = 20
        '
        'cboPaymentType
        '
        Me.cboPaymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaymentType.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaymentType.FormattingEnabled = True
        Me.cboPaymentType.Items.AddRange(New Object() {"CREDITO", "DEBITO", "EFECTIVO"})
        Me.cboPaymentType.Location = New System.Drawing.Point(901, 45)
        Me.cboPaymentType.Margin = New System.Windows.Forms.Padding(2)
        Me.cboPaymentType.Name = "cboPaymentType"
        Me.cboPaymentType.Size = New System.Drawing.Size(120, 28)
        Me.cboPaymentType.TabIndex = 19
        '
        'cboTipoEntrega
        '
        Me.cboTipoEntrega.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoEntrega.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoEntrega.FormattingEnabled = True
        Me.cboTipoEntrega.Items.AddRange(New Object() {"LOCAL", "DELIVERY"})
        Me.cboTipoEntrega.Location = New System.Drawing.Point(590, 45)
        Me.cboTipoEntrega.Margin = New System.Windows.Forms.Padding(2)
        Me.cboTipoEntrega.Name = "cboTipoEntrega"
        Me.cboTipoEntrega.Size = New System.Drawing.Size(123, 28)
        Me.cboTipoEntrega.TabIndex = 18
        '
        'txtFechaFin
        '
        Me.txtFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFechaFin.Location = New System.Drawing.Point(345, 45)
        Me.txtFechaFin.Margin = New System.Windows.Forms.Padding(2)
        Me.txtFechaFin.Mask = "##/##/####"
        Me.txtFechaFin.Name = "txtFechaFin"
        Me.txtFechaFin.PromptChar = Global.Microsoft.VisualBasic.ChrW(35)
        Me.txtFechaFin.Size = New System.Drawing.Size(92, 26)
        Me.txtFechaFin.TabIndex = 17
        '
        'txtFechaIni
        '
        Me.txtFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFechaIni.Location = New System.Drawing.Point(157, 45)
        Me.txtFechaIni.Margin = New System.Windows.Forms.Padding(2)
        Me.txtFechaIni.Mask = "##/##/####"
        Me.txtFechaIni.Name = "txtFechaIni"
        Me.txtFechaIni.PromptChar = Global.Microsoft.VisualBasic.ChrW(35)
        Me.txtFechaIni.Size = New System.Drawing.Size(97, 26)
        Me.txtFechaIni.TabIndex = 16
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(1036, 579)
        Me.btnBuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(155, 52)
        Me.btnBuscar.TabIndex = 15
        Me.btnBuscar.Text = "BUSCAR"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'statistics
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(45, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1284, 761)
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgEstadisticas)
        Me.Controls.Add(Me.cboPaymentType)
        Me.Controls.Add(Me.cboTipoEntrega)
        Me.Controls.Add(Me.txtFechaFin)
        Me.Controls.Add(Me.txtFechaIni)
        Me.Controls.Add(Me.btnBuscar)
        Me.MinimumSize = New System.Drawing.Size(1300, 800)
        Me.Name = "statistics"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ESTADISTICAS"
        CType(Me.dgEstadisticas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnLimpiar As Button
    Friend WithEvents txtTotal As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents dgEstadisticas As DataGridView
    Friend WithEvents cboPaymentType As ComboBox
    Friend WithEvents cboTipoEntrega As ComboBox
    Friend WithEvents txtFechaFin As MaskedTextBox
    Friend WithEvents txtFechaIni As MaskedTextBox
    Friend WithEvents btnBuscar As Button
End Class
