﻿Imports System.Data.SqlClient
Imports Spire.Doc
Imports Spire.Doc.Documents
Imports Spire.Doc.Fields
Imports HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment



Public Class modifySales


    Dim con As New SqlConnection
    Dim cmd As New SqlCommand
    Dim builder As SqlCommandBuilder
    Dim adap As SqlDataAdapter
    Dim dt As DataTable
    Dim i As Integer


    Private Sub modifySales_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\colacionesAmuyen.mdf;Integrated Security=True;Connect Timeout=30"
        If con.State = ConnectionState.Open Then
            con.Close()
        End If
        con.Open()

        showSales()

        dgModificarVenta.Columns(0).ReadOnly = True
        dgModificarVenta.Columns(1).ReadOnly = True
        dgModificarVenta.Columns(5).ReadOnly = True
    End Sub



    Private Sub showSales()
        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "Select so.id as ID,so.date as FECHA, so.delivery as ENTREGA,so.paymentType as PAGO, so.clientName as CLIENTE,  CASE
            WHEN delivery = 'delivery'
               THEN sum(sod.total) + 1000
               ELSE sum(sod.total)
       END as TOTAL  from sale_order so 
  Join sale_order_details sod
On so.id = sod.sale_order_id
Where status = 1
Group by so.id, so.date,so.delivery,so.paymentType,so.clientName"

        cmd.ExecuteNonQuery()
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt)
        dgModificarVenta.DataSource = dt

    End Sub

    Private Sub btnEliminarVenta_Click(sender As Object, e As EventArgs) Handles btnEliminarVenta.Click

        If MessageBox.Show("SEGURO QUE DESEA ELIMINAR?", "ELIMINAR LA VENTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.OK Then
            cmd = con.CreateCommand()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "UPDATE sale_order SET status = 0  WHERE id = '" & dgModificarVenta.CurrentRow.Cells(0).Value & "'"
            cmd.ExecuteNonQuery()
            MessageBox.Show("VENTA ELIMINADA")
            showSales()
        End If
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "UPDATE sale_order SET paymentType  = '" & dgModificarVenta.CurrentRow.Cells(3).Value.ToString.ToUpper & "' , clientName = '" & dgModificarVenta.CurrentRow.Cells(4).Value & "', delivery = '" & dgModificarVenta.CurrentRow.Cells(2).Value & "'  WHERE id = '" & dgModificarVenta.CurrentRow.Cells(0).Value & "'"
        cmd.ExecuteNonQuery()

        MessageBox.Show("VENTA MODIFICADO")
        showSales()

    End Sub




    Public Function imprimir()

        'ruta
        Dim path As String
        path = "C:\\Users\\Public\\Documents\\COMANDA.docx"

        'Load Document
        Dim doc As New Document()
        doc.LoadFromFile(path)

        Dim myList As New List(Of String)()
        myList = datos_varios()

        If myList.Count > 0 Then

            Dim array As String() = myList.ToArray
            doc.Replace("[HORA]", array(0).ToString, False, True)
            doc.Replace("[CLIENTNAME]", array(1).ToString, False, True)
            doc.Replace("[ADDRESS]", array(2).ToString, False, True)
            If LCase(array(4).ToString()) = "local" Then
                doc.Replace("[TOTAL]", array(3).ToString, False, True)
            Else
                doc.Replace("[TOTAL]", Convert.ToInt32(array(3).ToString) + 1000, False, True)

            End If


            Dim selection As TextSelection = doc.FindString("[tabla]", True, True)
            Dim Range As TextRange = selection.GetAsOneRange()
            Dim Paragraph As Paragraph = Range.OwnerParagraph
            Dim Body As Body = Paragraph.OwnerTextBody
            Dim Index As Integer = Body.ChildObjects.IndexOf(Paragraph)
            Body.ChildObjects.Remove(Paragraph)
            Dim table As Table = doc.AddSection().AddTable(True)

            Dim Header() As String = {"NOMBRE DEL PRODUCTO", "AGREGADOS", "CANTIDAD", "DESCRIPCION"}
            Dim data As String(,) = datos_tabla()

            Dim dataLength = (Convert.ToInt32(data.Length) / 4) - 1

            table.ResetCells((Convert.ToInt32(data.Length) / 4) + 1, Header.Length)
            Body.ChildObjects.Insert(Index, table)

            'Header Row

            Dim FRow As TableRow = table.Rows(0)

            FRow.IsHeader = True

            'Row Height

            FRow.Height = 6
            'Header Format

            FRow.RowFormat.BackColor = Color.AliceBlue

            For i As Integer = 0 To Header.Length - 1

                'Cell Alignment

                Dim p As Paragraph = FRow.Cells(i).AddParagraph()

                FRow.Cells(i).CellFormat.VerticalAlignment = VerticalAlignment.Middle

                p.Format.HorizontalAlignment = HorizontalAlignment.Center

                'Data Format

                Dim TR As TextRange = p.AppendText(Header(i))

                TR.CharacterFormat.FontName = "arial"

                TR.CharacterFormat.FontSize = 4

                TR.CharacterFormat.TextColor = Color.Teal

                TR.CharacterFormat.Bold = True

            Next i

            'Data Row

            For r As Integer = 0 To dataLength

                Dim DataRow As TableRow = table.Rows(r + 1)

                'Row Height

                DataRow.Height = 8

                'C Represents Column.

                For c As Integer = 0 To 3

                    'Cell Alignment

                    DataRow.Cells(c).CellFormat.VerticalAlignment = VerticalAlignment.Middle

                    'Fill Data in Rows

                    Dim p2 As Paragraph = DataRow.Cells(c).AddParagraph()

                    Dim TR2 As TextRange = p2.AppendText(data(r, c))

                    'Format Cells

                    p2.Format.HorizontalAlignment = HorizontalAlignment.Left
                    TR2.CharacterFormat.FontName = "Calibri"
                    TR2.CharacterFormat.FontSize = 8
                    TR2.CharacterFormat.TextColor = Color.Black

                Next c

            Next r

            doc.Sections(0).Tables(0).Rows(0).Cells(0).Width = 20


            ''Save And Launch
            'doc.SaveToFile("COMANDA.PDF", Spire.Doc.FileFormat.PDF)
            'System.Diagnostics.Process.Start("COMANDA.PDF")

            doc.SaveToFile("C:\\Users\\Public\\Documents\\COMANDA.PDF", Spire.Doc.FileFormat.PDF)
            doc.Close()
            Dim PrintPDF As New ProcessStartInfo
            PrintPDF.UseShellExecute = True
            PrintPDF.Verb = "print"
            PrintPDF.WindowStyle = ProcessWindowStyle.Hidden
            PrintPDF.FileName = "C:\\Users\\Public\\Documents\\COMANDA.PDF"
            Process.Start(PrintPDF)


        End If

    End Function



    Public Function datos_tabla()
        Dim con2 As New SqlConnection

        con2.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\local.mdf;Integrated Security=True;Connect Timeout=30"
        If con2.State = ConnectionState.Open Then
            con2.Close()
        End If
        con2.Open()

        Dim contador As Integer = 0
        Dim cmd As New SqlCommand("select sod.quantity, sod.productName, sod.added, so.date, sod.description from sale_order so join sale_order_details sod on so.id = sod.sale_order_id WHERE so.id = '" & dgModificarVenta.CurrentRow.Cells(0).Value & "'", con2)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader
        While dr.Read
            contador += 1
        End While
        dr.Close()


        Dim data As String(,) = New String(contador - 1, 3) {}
        contador = 0
        dr = cmd.ExecuteReader
        While dr.Read

            data(contador, 0) = dr("productName").ToString
            data(contador, 1) = dr("added").ToString
            data(contador, 2) = dr("quantity").ToString
            data(contador, 3) = dr("description").ToString
            contador = contador + 1
        End While
        Return data
        dr.Close()

    End Function


    Public Function datos_varios()

        Dim con3 As New SqlConnection

        con3.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\local.mdf;Integrated Security=True;Connect Timeout=30"
        If con3.State = ConnectionState.Open Then
            con3.Close()
        End If
        con3.Open()



        Dim cmd As New SqlCommand("Select so.date, so.address,so.clientName, sum(sod.total) as total, so.delivery from sale_order so 
 Join sale_order_details sod
On so.id = sod.sale_order_id
WHERE so.id = '" & dgModificarVenta.CurrentRow.Cells(0).Value & "'
Group by so.date, so.address,so.clientName, so.delivery", con3)

        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader
        Dim myList As New List(Of String)()
        While dr.Read
            myList.Add(dr("date").ToString)
            myList.Add(dr("clientName").ToString)
            myList.Add(dr("address").ToString)
            myList.Add(dr("total").ToString)
            myList.Add(dr("delivery").ToString)
        End While
        Return myList
        dr.Close()

    End Function




End Class