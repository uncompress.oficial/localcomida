﻿Imports System.Data.SqlClient

Public Class statistics


    Dim con As New SqlConnection
    Dim cmd As New SqlCommand
    Dim i As Integer

    Private Sub statistics_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        con.ConnectionString = "Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\colacionesAmuyen.mdf;Integrated Security=True;Connect Timeout=30"
        If con.State = ConnectionState.Open Then
            con.Close()
        End If
        con.Open()
    End Sub


    Private Sub numerarFilas()
        If dgEstadisticas IsNot Nothing Then
            For Each r As DataGridViewRow In dgEstadisticas.Rows
                r.HeaderCell.Value = (r.Index + 1).ToString()
            Next
        End If
    End Sub



    Public Sub estadisticas()
        Dim query As String
        cmd = con.CreateCommand()
        cmd.CommandType = CommandType.Text


        query = "Select so.id as ID,so.date as FECHA, so.delivery as ENTREGA,so.paymentType as PAGO, so.clientName as CLIENTE,  CASE
            WHEN delivery = 'delivery'
               THEN sum(sod.total) + 1000
               ELSE sum(sod.total)
       END as TOTAL  from sale_order so
Join sale_order_details sod
On so.id = sod.sale_order_id
Where status = 1"


        Dim fechaInicio As String = ""
        Dim fechaFin As String = ""

        If txtFechaIni.Text.Length = 10 Then
            Dim iDate As DateTime = Convert.ToDateTime(txtFechaIni.Text)
            fechaInicio = iDate.Year.ToString + "/" + iDate.Month.ToString + "/" + iDate.Day.ToString
        End If

        If txtFechaFin.Text.Length = 10 Then
            Dim fDate As DateTime = Convert.ToDateTime(txtFechaFin.Text)
            fechaFin = fDate.Year.ToString + "/" + fDate.Month.ToString + "/" + fDate.Day.ToString

        End If
        If fechaInicio <> "" And fechaFin <> "" Then
            query += " and so.date BETWEEN '" + fechaInicio + "' AND '" + fechaFin + "'"
        ElseIf fechaInicio <> "" Then
            query += " and so.date > '" + fechaInicio + "'"
        ElseIf fechaFin <> "" Then

            query += " and so.date < '" + fechaFin + "'"
        End If


        If cboPaymentType.Text <> "" Then
            query += " And paymentType = '" + cboPaymentType.Text + "'"
        End If
        If cboTipoEntrega.Text <> "" Then
            query += " and delivery = '" + cboTipoEntrega.Text + "'"
        End If
        query += "Group by so.id, so.date,so.delivery,so.paymentType,so.clientName"
        cmd.CommandText = query
        cmd.ExecuteNonQuery()
        Dim dt As New DataTable()
        Dim da As New SqlDataAdapter(cmd)
        da.Fill(dt)
        dgEstadisticas.DataSource = dt

        numerarFilas()
        totalSales()


    End Sub



    Private Sub totalSales()
        Dim total As Integer = 0
        For index As Integer = 0 To dgEstadisticas.Rows.Count - 1
            If dgEstadisticas.Rows(index).Cells(5).Value.ToString <> "" Then
                total += dgEstadisticas.Rows(index).Cells(5).Value

            End If

        Next
        txtTotal.Text = total

    End Sub

    Private Sub btnLimpiar_Click(sender As Object, e As EventArgs) Handles btnLimpiar.Click
        cboPaymentType.SelectedIndex = -1
        cboTipoEntrega.SelectedIndex = -1
        txtFechaIni.Text = ""
        txtFechaFin.Text = ""
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        estadisticas()
    End Sub

    Private Sub txtFechaIni_MaskInputRejected(sender As Object, e As MaskInputRejectedEventArgs) Handles txtFechaIni.MaskInputRejected
        txtFechaIni.Select(0, 0)
    End Sub

    Private Sub txtFechaFin_MaskInputRejected(sender As Object, e As MaskInputRejectedEventArgs) Handles txtFechaFin.MaskInputRejected
        txtFechaFin.Select(0, 0)
    End Sub
End Class