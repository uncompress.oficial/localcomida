﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class modifySales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnEliminarVenta = New System.Windows.Forms.Button()
        Me.dgModificarVenta = New System.Windows.Forms.DataGridView()
        CType(Me.dgModificarVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(1103, 421)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(116, 62)
        Me.btnImprimir.TabIndex = 7
        Me.btnImprimir.Text = "IMPRIMIR"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(627, 421)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(116, 62)
        Me.btnUpdate.TabIndex = 6
        Me.btnUpdate.Text = "ACTUALIZAR"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnEliminarVenta
        '
        Me.btnEliminarVenta.Location = New System.Drawing.Point(103, 421)
        Me.btnEliminarVenta.Name = "btnEliminarVenta"
        Me.btnEliminarVenta.Size = New System.Drawing.Size(116, 62)
        Me.btnEliminarVenta.TabIndex = 5
        Me.btnEliminarVenta.Text = "ELIMINAR"
        Me.btnEliminarVenta.UseVisualStyleBackColor = True
        '
        'dgModificarVenta
        '
        Me.dgModificarVenta.AllowUserToAddRows = False
        Me.dgModificarVenta.AllowUserToDeleteRows = False
        Me.dgModificarVenta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgModificarVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgModificarVenta.Location = New System.Drawing.Point(103, 63)
        Me.dgModificarVenta.Name = "dgModificarVenta"
        Me.dgModificarVenta.Size = New System.Drawing.Size(1116, 280)
        Me.dgModificarVenta.TabIndex = 4
        '
        'modifySales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(45, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1300, 800)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.btnEliminarVenta)
        Me.Controls.Add(Me.dgModificarVenta)
        Me.MinimumSize = New System.Drawing.Size(1300, 800)
        Me.Name = "modifySales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MODIFCAR VENTAS"
        CType(Me.dgModificarVenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnImprimir As Button
    Friend WithEvents btnUpdate As Button
    Friend WithEvents btnEliminarVenta As Button
    Friend WithEvents dgModificarVenta As DataGridView
End Class
