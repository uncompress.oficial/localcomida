﻿Public Class FormMenu





    Private Sub FormMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        hideSubmenu()

    End Sub

    Private Sub hideSubmenu()
        PanelSalesSubmenu.Visible = False
        PanelAddSubmenu.Visible = False
        PanelModifySubmenu.Visible = False
    End Sub

    Private Sub showSubmenu(submenu As Panel)

        If submenu.Visible = False Then
            hideSubmenu()
            submenu.Visible = True
        Else
            submenu.Visible = False
        End If

    End Sub

    Private Sub btnSales_Click(sender As Object, e As EventArgs) Handles btnSales.Click
        showSubmenu(PanelSalesSubmenu)
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        showSubmenu(PanelAddSubmenu)

    End Sub

    Private Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        showSubmenu(PanelModifySubmenu)
    End Sub

    Private Sub btnMakeSales_Click(sender As Object, e As EventArgs) Handles btnMakeSales.Click
        makeSales.Show()


        'El codigo debe ser escrito sobre el sub hideSubmenu()
        hideSubmenu()
    End Sub

    Private Sub btnModifySales_Click(sender As Object, e As EventArgs) Handles btnModifySales.Click
        modifySales.Show()


        'El codigo debe ser escrito sobre el sub hideSubmenu()
        hideSubmenu()
    End Sub

    Private Sub btnAddProduct_Click(sender As Object, e As EventArgs) Handles btnAddProduct.Click
        addProducts.Show()



        'El codigo debe ser escrito sobre el sub hideSubmenu()
        hideSubmenu()
    End Sub

    Private Sub btnAddOffer_Click(sender As Object, e As EventArgs) Handles btnAddOffer.Click
        addOffers.Show()


        'El codigo debe ser escrito sobre el sub hideSubmenu()
        hideSubmenu()
    End Sub

    Private Sub btnModifyProducts_Click(sender As Object, e As EventArgs) Handles btnModifyProducts.Click
        modifyProducts.Show()
        'El codigo debe ser escrito sobre el sub hideSubmenu()
        hideSubmenu()
    End Sub

    Private Sub btnModifyOffers_Click(sender As Object, e As EventArgs) Handles btnModifyOffers.Click
        modifyOffers.Show()



        'El codigo debe ser escrito sobre el sub hideSubmenu()
        hideSubmenu()
    End Sub

    Private Sub btnStatistics_Click(sender As Object, e As EventArgs) Handles btnStatistics.Click
        statistics.Show()




        'El codigo debe ser escrito sobre el sub hideSubmenu()
        hideSubmenu()
    End Sub

    Private Sub horaFecha_Tick(sender As Object, e As EventArgs) Handles horaFecha.Tick
        Me.lblhora.Text = DateTime.Now.ToString("HH:mm:ss")
        Me.lblFecha.Text = DateTime.Now.ToString("dddd dd MMMM yyyy")
    End Sub

End Class
